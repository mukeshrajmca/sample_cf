package com.bt.generichub.dao;

import com.bt.generichub.dto.Hub;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
@SuppressWarnings("unused")
public class GenericHubJdbcRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Hub findByScode(Hub hub) {
        return jdbcTemplate.queryForObject("select * from PMF_HUB_SCENARIO_MV where PROMOTION_SCODE=? and SCENARIO=?", new Object[]{hub.getPromotionScode(), hub.getScenario()},
                new BeanPropertyRowMapper<>(Hub.class));
    }

    public List<Hub> findHubs(Hub hub) {
        List<Hub> hubs = new ArrayList<>();
        List<Hub> rows = jdbcTemplate.query("select * from PMF_HUB_SCENARIO_MV where PROMOTION_SCODE=? and SCENARIO=?", new Object[]{hub.getPromotionScode(), hub.getScenario()},
                new BeanPropertyRowMapper<>(Hub.class));
        for (Hub row : rows) {
            Hub hubRs = new Hub();
            hubRs.setPromotionScode(row.getPromotionScode());
            hubRs.setHubScode(row.getHubScode());
            hubRs.setScenario(row.getScenario());
            hubRs.setSelected(row.getSelected());
            hubs.add(hubRs);
        }

        return hubs;
    }

    class HubRowMapper implements RowMapper<Hub> {
        @Override
        public Hub mapRow(ResultSet rs, int rowNum) throws SQLException {
            Hub hub = new Hub();
            hub.setPromotionScode(rs.getString("PROMOTION_SCODE"));
            hub.setHubScode(rs.getString("HUB_SCODE"));
            hub.setScenario(rs.getString("SCENARIO"));
            hub.setSelected(rs.getString("SELECTED"));
            return hub;
        }

    }

}
