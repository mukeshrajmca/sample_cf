package com.bt.generichub;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
@SuppressWarnings("unused")
public class WebServiceConfig {

    @Bean
    public ServletRegistrationBean<MessageDispatcherServlet> messageDispatcherServlet(ApplicationContext context) {
        MessageDispatcherServlet messageDispatcherServlet = new MessageDispatcherServlet();
        messageDispatcherServlet.setApplicationContext(context);
        messageDispatcherServlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean<>(messageDispatcherServlet, "/appsGenericHubService/*");
    }

    @Bean(name = "genericHub")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema genericHubSchema) {
        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setPortTypeName("GenericHubPort");
        definition.setTargetNamespace("http://bt.com/generichub");
        definition.setLocationUri("/ws");
        definition.setSchema(genericHubSchema);
        return definition;
    }

    @Bean
    public XsdSchema genericHubSchema() {
        return new SimpleXsdSchema(new ClassPathResource("generic-hub.xsd"));
    }
}