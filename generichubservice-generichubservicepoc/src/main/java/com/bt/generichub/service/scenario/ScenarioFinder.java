package com.bt.generichub.service.scenario;

import com.bt.generichub.dto.ServiceRequest;

public interface ScenarioFinder {

    Integer findScenario(ServiceRequest serviceRequest);

}
