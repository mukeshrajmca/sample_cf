package com.bt.generichub.service.slt;

import com.bt.generichub.parser.yaml.slt.SltYamlParser;
import com.google.common.collect.ImmutableMap;
import org.springframework.context.annotation.Configuration;

@Configuration
@SuppressWarnings("unused")
public class SltGroupFinderImpl implements SltGroupFinder {

    private final ImmutableMap<String, String> bbSlt;

    public SltGroupFinderImpl() {
        this.bbSlt = new SltYamlParser().constructScenarioMap();
    }

    @Override
    public String getSltGroup(String slt) {
        return bbSlt.get(slt);
    }
}
