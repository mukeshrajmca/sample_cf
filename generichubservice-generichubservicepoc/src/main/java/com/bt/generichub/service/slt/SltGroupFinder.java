package com.bt.generichub.service.slt;

public interface SltGroupFinder {

    String getSltGroup(String slt);
}
