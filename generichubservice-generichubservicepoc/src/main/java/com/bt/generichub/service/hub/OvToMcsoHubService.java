package com.bt.generichub.service.hub;

public interface OvToMcsoHubService {

    String findMcsoHubName(String ovHubName);
}
