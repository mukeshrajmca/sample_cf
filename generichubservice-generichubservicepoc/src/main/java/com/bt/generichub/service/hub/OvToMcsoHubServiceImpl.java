package com.bt.generichub.service.hub;

import com.bt.generichub.parser.yaml.hub.HubYamlParser;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OvToMcsoHubServiceImpl implements OvToMcsoHubService {
    private final ImmutableMap<String, String> OV_TO_MCSO_HUB_MAP;

    public OvToMcsoHubServiceImpl() {
        this.OV_TO_MCSO_HUB_MAP = new HubYamlParser().constructPmfToMcsoHubMap();
    }

    @Override
    public String findMcsoHubName(String ovHubName) {
        String hub = OV_TO_MCSO_HUB_MAP.get(ovHubName);
        Preconditions.checkNotNull(hub, "Hub not Found in OV to MCSO mapping");
        return hub;

    }
}
