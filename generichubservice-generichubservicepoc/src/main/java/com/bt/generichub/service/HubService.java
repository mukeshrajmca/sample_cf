package com.bt.generichub.service;

import com.bt.generichub.dao.GenericHubJdbcRepository;
import com.bt.generichub.dto.GenericHub;
import com.bt.generichub.dto.Hub;
import com.bt.generichub.dto.ServiceRequest;
import com.bt.generichub.service.scenario.ScenarioFinder;
import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class HubService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());


    public GenericHub getHubs(ServiceRequest serviceRequest) {

        GenericHub genericHub = new GenericHub();
        genericHub.setHubScode(serviceRequest.getTargetSlt());
        genericHub.setDefaultHub(serviceRequest.getTargetProductFamily());
        genericHub.setInContract(serviceRequest.getTargetPromotion());

        return genericHub;
    }

    public List<GenericHub> getAllHubs(ServiceRequest serviceRequest, GenericHubJdbcRepository repository, Environment env,
                                       ScenarioFinder scenarioFinder) {


        logger.info("---incoming request:" +
                "targetPromo:" + serviceRequest.getTargetPromotion() + "," +
                "targetProductFamily:" + serviceRequest.getTargetProductFamily() + "," +
                "targetSlt:" + serviceRequest.getTargetSlt() + "," +
                "existingHub:" + serviceRequest.getExistingHub() + "," +
                "callingSystem:" + serviceRequest.getCallingSystem() + ","
        );

        //hardcode for now
        Integer scenario = scenarioFinder.findScenario(serviceRequest);
        Preconditions.checkNotNull(scenario, "Scenario cannot be null");

        Hub hub = new Hub();
        hub.setPromotionScode(serviceRequest.getTargetPromotion());
        hub.setScenario(scenario.toString());

        List<Hub> outHubs = repository.findHubs(hub);

        List<GenericHub> genericHubList = new ArrayList<>();

        outHubs.forEach(outHubItem -> {
            GenericHub genericHub = new GenericHub();
            genericHub.setHubScode(outHubItem.getHubScode());
            genericHub.setDefaultHub(outHubItem.getSelected());
            genericHub.setScenario(outHubItem.getScenario());
            genericHub.setInContract("N");

            genericHubList.add(genericHub);
        });

        return genericHubList;
    }


}
