package com.bt.generichub.service.scenario;

import com.bt.generichub.dto.ServiceRequest;
import com.bt.generichub.dto.scenarios.ScenarioKey;
import com.bt.generichub.parser.yaml.scenario.ScenarioYamlParser;
import com.google.common.collect.ImmutableMap;
import org.springframework.context.annotation.Configuration;

@Configuration
@SuppressWarnings("WeakerAccess")
public class ScenarioFinderImpl implements ScenarioFinder {

    private final ImmutableMap<ScenarioKey, Integer> scenarioMapBT;

    public ScenarioFinderImpl() {
        this.scenarioMapBT = new ScenarioYamlParser().constructScenarioMap();
    }

    @Override
    public Integer findScenario(ServiceRequest serviceRequest) {
        ScenarioKey scenarioKey = new ScenarioKey(serviceRequest.getTargetProductFamily(), serviceRequest.getProductKey(),
                serviceRequest.getTargetSlt(), serviceRequest.getExistingHub());
        return scenarioMapBT.get(scenarioKey);
    }
}