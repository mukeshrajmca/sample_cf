package com.bt.generichub;

import com.bt.generichub.dao.GenericHubJdbcRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@SuppressWarnings("unused")
public class GenericHubServiceApplication implements CommandLineRunner {

    @Autowired
    GenericHubJdbcRepository repository;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public static void main(String[] args) {
        SpringApplication.run(GenericHubServiceApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

    }
}
