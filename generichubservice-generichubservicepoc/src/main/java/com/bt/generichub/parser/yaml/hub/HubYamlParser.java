package com.bt.generichub.parser.yaml.hub;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public class HubYamlParser {

    private static final String PMF_TO_MCSO_HUB_YAML = "PmfToMcsoHub.yaml";

    public ImmutableMap<String, String> constructPmfToMcsoHubMap() {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

        try {
            InputStream yamlFileInputStream = getClass().getClassLoader().getResourceAsStream(PMF_TO_MCSO_HUB_YAML);
            Preconditions.checkNotNull(yamlFileInputStream,
                    "No Yaml File found in resources File: " + PMF_TO_MCSO_HUB_YAML);
            TypeReference<Map<String, String>> mapTypeReference = new TypeReference<Map<String, String>>() {
            };
            Map<String, String> sltMap = mapper.readValue(yamlFileInputStream, mapTypeReference);
            return ImmutableMap.copyOf(sltMap);
        } catch (JsonParseException | JsonMappingException e) {//TODO handle the errors
            throw new RuntimeException("Error while parsing YAML" + e);
        } catch (IOException e) {
            throw new RuntimeException("IOError while parsing YAML" + e);
        }
    }
}
