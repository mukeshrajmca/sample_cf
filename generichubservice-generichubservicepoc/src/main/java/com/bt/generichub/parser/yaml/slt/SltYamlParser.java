package com.bt.generichub.parser.yaml.slt;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public class SltYamlParser {

    private static final String GENERIC_HUB_BBSLT_YAML = "GenericHubBBSlt.yaml";

    public ImmutableMap<String, String> constructScenarioMap() {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

        try {
            InputStream yamlFileInputStream = getClass().getClassLoader().getResourceAsStream(GENERIC_HUB_BBSLT_YAML);
            Preconditions.checkNotNull(yamlFileInputStream,
                    "No Yaml File found in resources File: " + GENERIC_HUB_BBSLT_YAML);
            TypeReference<Map<String, String>> mapTypeReference = new TypeReference<Map<String, String>>() {
            };
            Map<String, String> sltMap = mapper.readValue(yamlFileInputStream, mapTypeReference);
            return ImmutableMap.copyOf(sltMap);
        } catch (JsonParseException | JsonMappingException e) {//TODO handle the errors
            throw new RuntimeException("Error while parsing YAML" + e);
        } catch (IOException e) {
            throw new RuntimeException("IOError while parsing YAML" + e);
        }
    }
}
