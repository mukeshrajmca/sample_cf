package com.bt.generichub.parser.yaml.scenario;

import com.bt.generichub.dto.scenarios.CustomerScenario;
import com.bt.generichub.dto.scenarios.ScenarioKey;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class ScenarioYamlParser {

    private static final String GENERIC_HUB_SCENARIOS_YAML = "GenericHubScenarios.yaml";

    // TODO: The method is very crude need to handle all the error scenarios and proper error reporting is pending
    public ImmutableMap<ScenarioKey, Integer> constructScenarioMap() {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

        try {
            InputStream yamlFileInputStream = getClass().getClassLoader().getResourceAsStream(GENERIC_HUB_SCENARIOS_YAML);
            Preconditions.checkNotNull(yamlFileInputStream,
                    "No Yaml File found in resources File: " + GENERIC_HUB_SCENARIOS_YAML);
            Preconditions.checkNotNull(yamlFileInputStream);
            TypeReference<List<CustomerScenario>> scenariosListTypeReference = new TypeReference<List<CustomerScenario>>() {
            };
            List<CustomerScenario> customerScenariosList = mapper.readValue(yamlFileInputStream,
                    scenariosListTypeReference);

            ImmutableMap.Builder<ScenarioKey, Integer> scenariosMap = ImmutableMap.builder();

            for (CustomerScenario customerScenario : customerScenariosList) {
                String productKey = customerScenario.getProductKey();
                int scenario = customerScenario.getScenario();
                String slt = customerScenario.getSltGroup();
                for (String family : customerScenario.getProductFamily()) {
                    for (String yFlag : customerScenario.getyFlag()) {
                        scenariosMap.put(new ScenarioKey(family, productKey, slt, yFlag), scenario);
                    }
                }
            }
            return scenariosMap.build();
        } catch (JsonParseException | JsonMappingException e) {//TODO handle the errors
            throw new RuntimeException("Error while parsing YAML" + e);
        } catch (IOException e) {
            throw new RuntimeException("IOError while parsing YAML" + e);
        }
    }

}
