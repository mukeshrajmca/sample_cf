/*
 *
 */
package com.bt.generichub.controller;

/*
 * @author Abhijit P
 * This is a sample controller class to handle get request
 */


import com.bt.generichub.dao.GenericHubJdbcRepository;
import com.bt.generichub.dto.GenericHub;
import com.bt.generichub.dto.ServiceRequest;
import com.bt.generichub.service.HubService;
import com.bt.generichub.service.hub.OvToMcsoHubService;
import com.bt.generichub.service.scenario.ScenarioFinder;
import com.bt.generichub.service.slt.SltGroupFinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@SuppressWarnings("unused")
public class GenericHubRestController {

    @Autowired
    private GenericHubJdbcRepository repository;

    @Autowired
    private Environment env;

    @Autowired
    private ScenarioFinder scenarioFinder;

    @Autowired
    private SltGroupFinder sltGroupFinder;

    @GetMapping("/appsGenericHubRestService/getHubs")
    @ResponseBody
    public GenericHubRestResponse welcomeUser(@RequestParam(name = "targetSlt", required = false, defaultValue = "") String targetSlt,
                                              @RequestParam(name = "targetProductFamily", required = false, defaultValue = "") String targetProductFamily,
                                              @RequestParam(name = "targetPromotion", required = false, defaultValue = "") String targetPromotion,
                                              @RequestParam(name = "existingHub", required = false, defaultValue = "") String existingHub,
                                              @RequestParam(name = "callingSystem", required = false, defaultValue = "Online") String callingSystem) {

        //set the req params into the dto
        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setTargetSlt(sltGroupFinder.getSltGroup(targetSlt));
        serviceRequest.setTargetProductFamily(targetProductFamily);
        serviceRequest.setTargetPromotion(targetPromotion);
        serviceRequest.setExistingHub(existingHub);
        serviceRequest.setCallingSystem(callingSystem);

        serviceRequest.setProductKey("DEFAULT"); //TODO these must not be hardcoded

        //invoke the service
        HubService hubService = new HubService();
        List<GenericHub> genericHub = hubService.getAllHubs(serviceRequest, repository, env, scenarioFinder);

        //set the retrieved hubs into the response and send in rest response
        return new GenericHubRestResponse(genericHub);
    }

}
