package com.bt.generichub.controller;

import com.bt.generichub.dto.GenericHub;

import java.util.List;

@SuppressWarnings({"unused", "WeakerAccess"})
public class GenericHubRestResponse {

    private final List<GenericHub> genericHub;

    public GenericHubRestResponse(List<GenericHub> genericHub) {
        this.genericHub = genericHub;
    }

    public List<GenericHub> getGenericHub() {
        return genericHub;
    }

}
