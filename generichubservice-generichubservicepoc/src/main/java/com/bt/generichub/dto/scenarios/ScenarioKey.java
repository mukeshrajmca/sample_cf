package com.bt.generichub.dto.scenarios;

import java.util.Objects;

@SuppressWarnings("unused")
public class ScenarioKey {

    private final String product_family;
    private final String productKey;
    private final String slt;
    private final String y_flag;

    public ScenarioKey(String product_family, String productKey, String slt, String y_flag) {
        this.product_family = product_family;
        this.productKey = productKey;
        this.slt = slt;
        this.y_flag = y_flag;
    }

    public String getProduct_family() {
        return product_family;
    }

    public String getProductKey() {
        return productKey;
    }

    public String getSlt() {
        return slt;
    }

    public String getY_flag() {
        return y_flag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ScenarioKey that = (ScenarioKey) o;
        return Objects.equals(product_family, that.product_family) &&
                Objects.equals(productKey, that.productKey) &&
                Objects.equals(slt, that.slt) &&
                Objects.equals(y_flag, that.y_flag);
    }

    @Override
    public int hashCode() {
        return Objects.hash(product_family, productKey, slt, y_flag);
    }

    @Override
    public String toString() {
        return "ScenarioKey{" +
                "product_family='" + product_family + '\'' +
                ", productKey='" + productKey + '\'' +
                ", slt='" + slt + '\'' +
                ", y_flag='" + y_flag + '\'' +
                '}';
    }
}
