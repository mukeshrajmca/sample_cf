package com.bt.generichub.dto;

@SuppressWarnings("unused")
public class ServiceRequest {
    private String targetSlt;
    private String targetProductFamily;
    private String targetPromotion;
    private String existingHub;
    private String callingSystem;
    private String productKey;

    public ServiceRequest() {
    }

    public String getTargetSlt() {
        return targetSlt;
    }

    public void setTargetSlt(String value) {
        this.targetSlt = value;
    }

    public String getTargetProductFamily() {
        return targetProductFamily;
    }

    public void setTargetProductFamily(String value) {
        this.targetProductFamily = value;
    }

    public String getTargetPromotion() {
        return targetPromotion;
    }

    public void setTargetPromotion(String value) {
        this.targetPromotion = value;
    }

    public String getExistingHub() {
        return existingHub;
    }

    public void setExistingHub(String value) {
        this.existingHub = value;
    }

    public String getCallingSystem() {
        return callingSystem;
    }

    public void setCallingSystem(String value) {
        this.callingSystem = value;
    }

    public String getProductKey() {
        return productKey;
    }

    public void setProductKey(String productKey) {
        this.productKey = productKey;
    }
}
