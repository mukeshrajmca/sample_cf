package com.bt.generichub.dto.scenarios;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@SuppressWarnings("unused")
public class CustomerScenario {

    @JsonProperty("productFamily")
    private List<String> productFamily;
    @JsonProperty("productKey")
    private String productKey;
    @JsonProperty("scenario")
    private int scenario;
    @JsonProperty("sltGroup")
    private String sltGroup;
    @JsonProperty("yFlag")
    private List<String> yFlag;

    public CustomerScenario() {

    }

    public CustomerScenario(List<String> productFamily, String productKey, int scenario, String sltGroup,
                            List<String> yFlag) {
        this.productFamily = productFamily;
        this.productKey = productKey;
        this.scenario = scenario;
        this.sltGroup = sltGroup;
        this.yFlag = yFlag;
    }

    public List<String> getProductFamily() {
        return productFamily;
    }

    public String getProductKey() {
        return productKey;
    }

    public int getScenario() {
        return scenario;
    }

    public String getSltGroup() {
        return sltGroup;
    }

    public List<String> getyFlag() {
        return yFlag;
    }

    @Override
    public String toString() {
        return "CustomerScenario{" +
                "productFamily=" + productFamily +
                ", productKey='" + productKey + '\'' +
                ", scenario=" + scenario +
                ", sltGroup='" + sltGroup + '\'' +
                ", yFlag=" + yFlag +
                '}';
    }
}
