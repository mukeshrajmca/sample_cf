package com.bt.generichub.dto;

public class Hub {

    public Hub() {
        super();
    }

    private String promotionScode;
    private String hubScode;
    private String scenario;
    private String selected;

    public String getPromotionScode() {
        return promotionScode;
    }

    public void setPromotionScode(String promotionScode) {
        this.promotionScode = promotionScode;
    }

    public String getHubScode() {
        return hubScode;
    }

    public void setHubScode(String hubScode) {
        this.hubScode = hubScode;
    }

    public String getScenario() {
        return scenario;
    }

    public void setScenario(String scenario) {
        this.scenario = scenario;
    }

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }
}
