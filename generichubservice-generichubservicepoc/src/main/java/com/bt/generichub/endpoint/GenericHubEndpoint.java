package com.bt.generichub.endpoint;

import com.bt.generichub.dao.GenericHubJdbcRepository;
import com.bt.generichub.dto.GenericHub;
import com.bt.generichub.dto.GenericHubRequest;
import com.bt.generichub.dto.GenericHubResponse;
import com.bt.generichub.dto.ServiceRequest;
import com.bt.generichub.service.HubService;
import com.bt.generichub.service.hub.OvToMcsoHubService;
import com.bt.generichub.service.scenario.ScenarioFinder;
import com.bt.generichub.service.slt.SltGroupFinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.util.StringUtils;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;

@Endpoint
@SuppressWarnings("unused")
public class GenericHubEndpoint {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private GenericHubJdbcRepository repository;

    @Autowired
    private Environment env;

    @Autowired
    private ScenarioFinder scenarioFinder;

    @Autowired
    private SltGroupFinder sltGroupFinder;

    @Autowired
    private OvToMcsoHubService ovToMcsoHubService;

    @PayloadRoot(namespace = "http://bt.com/generichub/dto", localPart = "GenericHubRequest")
    @ResponsePayload
    public GenericHubResponse processAllHubsRequest(@RequestPayload GenericHubRequest request) {

        logger.info("--------processAllHubsRequest--------" + env.getProperty("server.port"));

        //set the req params into the dto
        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setTargetSlt(sltGroupFinder.getSltGroup(request.getTargetSlt()));
        serviceRequest.setTargetProductFamily(request.getTargetProductFamily());
        serviceRequest.setTargetPromotion(request.getTargetPromotion());
        serviceRequest.setExistingHub(
                StringUtils.isEmpty(request.getExistingHub()) ? "noHubFlag" :
                        ovToMcsoHubService.findMcsoHubName(request.getExistingHub()));
        serviceRequest.setCallingSystem(request.getCallingSystem());
        serviceRequest.setProductKey("DEFAULT"); //TODO these must not be hadcoded

        //invoke the service
        HubService hubService = new HubService();
        List<GenericHub> genericHub = hubService.getAllHubs(serviceRequest, repository, env, scenarioFinder);

        //set the retrieved hubs into the response and send in soap response
        GenericHubResponse response = new GenericHubResponse();
        genericHub.forEach(genericHubItem -> response.getGenericHub().add(genericHubItem));

        return response;
    }

}