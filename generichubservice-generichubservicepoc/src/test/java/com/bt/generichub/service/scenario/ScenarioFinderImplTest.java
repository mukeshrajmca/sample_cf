package com.bt.generichub.service.scenario;

import com.bt.generichub.dto.ServiceRequest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ScenarioFinderImplTest {

    @Test
    public void findScenario() {

        ScenarioFinder instance = new ScenarioFinderImpl();
        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setTargetProductFamily("BROADBAND");
        serviceRequest.setProductKey("DEFAULT");
        serviceRequest.setTargetSlt("fttc_slt");
        serviceRequest.setExistingHub("hasVoyager");

        assertEquals(new Integer(1), instance.findScenario(serviceRequest));

    }
}